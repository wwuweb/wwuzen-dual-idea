#!/bin/bash

# Set error options for bash
set -o errexit
set -o nounset
set -o pipefail

# Real directory of the script, not current working directory
readonly SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)

# Color escape sequences for coloring shell output
readonly GREEN='\033[0;32m'
readonly NC='\033[0m'

# Exclude search from directory
readonly EXCLUDE='.git'

# Print a message when exiting, possibly performing cleanup
__exitHandler() {
  wait

  local status=$?

  echo "Exiting"
  exit $status
}

# Trap errors and exit signals
trap __exitHandler \
  ERR EXIT SIGINT SIGTERM SIGQUIT

__main() {
  # Target text that will be searched and replaced
  local target=

  # Text with which target will be replaced
  local replacement=

  # Get the target text from the user
  read -p "Target: " target

  # Get the replacmeent text from the user
  read -p "Replacement: " replacement

  # Test if the .info file exists to rename
  if [ ! -e "${target}.info" ]; then
    echo "${target}.info does not exist"
    exit 1
  fi

  # Output operations that will be performed
  echo
  echo -e "${GREEN}Will rename '${target}.info' to '${replacement}.info'${NC}"
  echo
  echo -e "${GREEN}Will replace '${target}' in the following locations:${NC}"
  grep --exclude-dir="${EXCLUDE}" --recursive --color=auto "${target}" .
  echo

  local proceed=
  # Prompt the user to continue the replacement
  read -p "Continue with replacement? [Y/n]: " proceed

  if [ ! $proceed == "Y" ]; then
    exit 2
  fi

  # Rename the .info file
  mv "${target}.info" "${replacement}.info"

  # Replacmenet expression for sed command
  local expression='s/'"${target}"'/'"${replacement}"'/g'

  # Find all php files recusively in the directory and run sed to replace the
  # given patterns
  find \
    . \
    \( -name "${EXCLUDE}" -prune \) -o \
    -type f \
    -exec sed \
    --in-place \
    --expression="${expression}" {} +

  # Output results of the operation
  echo
  echo -e "${GREEN}Renamed '${target}.info' to '${replacement}.info'${NC}"
  echo
  echo -e "${GREEN}Replaced '${target}' with '${replacement}':${NC}"
  grep --exclude-dir="${EXCLUDE}" --recursive --color=auto "${replacement}" .
  echo

  exit 0
}

__main $@
